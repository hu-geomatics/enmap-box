import pathlib
import unittest
import site
PATH_REPO = pathlib.Path(r'D:\Repositories\enmap-box')
assert PATH_REPO.is_dir()
PATH_TESTS = PATH_REPO / 'tests' / 'src'
site.addsitedir(PATH_TESTS)

try:
    import enmapbox
except ModuleNotFoundError:
    site.addsitedir(PATH_REPO)
    site.addsitedir(PATH_REPO / 'site-packages')
    import enmapbox

# use the default shared TestLoader instance
test_loader = unittest.defaultTestLoader
pattern = 'test*.py'

# use the basic test runner that outputs to sys.stderr
test_runner = unittest.TextTestRunner()

# automatically discover all tests in the current dir of the form test*.py
# NOTE: only works for python 2.7 and later
test_suite = test_loader.discover(PATH_TESTS, pattern=pattern)

# run the test suite
test_runner.run(test_suite)