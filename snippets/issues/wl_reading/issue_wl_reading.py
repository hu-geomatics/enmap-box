
import pathlib
import datetime
from qgis.core import QgsRasterLayer

from enmapboxprocessing.rasterreader import RasterReader
from enmapbox.qgispluginsupport.qps.qgsrasterlayerproperties import QgsRasterLayerSpectralProperties

path = pathlib.Path(__file__).parent / 'asd.tif'
assert path.is_file()

lyr = QgsRasterLayer(path.as_posix())
reader = RasterReader(path.as_posix())

print(f'Reading QGISPAM wavelength from {path}\n'
      f'{lyr.bandCount()}x{lyr.height()}x{lyr.width()}')

tA = datetime.datetime.now()
wavelengthA = [reader.wavelength(i + 1) for i in range(reader.bandCount())]
tA = datetime.datetime.now() - tA
del reader

tB = datetime.datetime.now()
props = QgsRasterLayerSpectralProperties.fromRasterLayer(lyr)
wavelengthB = props.wavelengths()
tB = datetime.datetime.now() - tB

assert len(wavelengthA) == len(wavelengthB)
for wa, wb in zip(wavelengthA, wavelengthB):
    assert wa == wb
print(f'RasterReader: {tA}\n'
      f'QgsRasterLayerSpectralProperties: {tB}')
