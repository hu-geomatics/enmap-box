Mark this directory as a Source Root and start the EnMAP-Box:
>>>enmapBox = EnMAPBox(load_core_apps=False, load_other_apps=False)  # don't load apps to avoid optional imports

If you encounter any ModuleNotFoundErrors, we probably have some top level imports that need to be moved into the methods,
where those dependencies are needed.
